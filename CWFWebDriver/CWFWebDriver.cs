﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;

namespace CWFWebDriver
{
    [Guid("086872AB-FEC3-41A8-858C-9B3151AD64A0")]
    internal interface ICWFWebDriver
    {
        [DispId(1)]
        void OpenBrowser();
        void CloseBrowser();
        void Navigate(string url);
        string PageSource();
    }

    [Guid("4666B36A-2B73-418C-8176-D5C9CDBA6711"), InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ICWFWebDriverEvents
    {

    }

    [Guid("CFAFBB5E-3CAA-474D-8294-A3DE62009766"), ClassInterface(ClassInterfaceType.None), ComSourceInterfaces(typeof(ICWFWebDriverEvents))]
    public class Chrome: ICWFWebDriver
    {
        private IWebDriver webDriver;

        public void OpenBrowser()
        {
            webDriver = new ChromeDriver();
        }

        public void CloseBrowser()
        {
            webDriver.Quit();
        }

        public void Navigate(string url)
        {
            webDriver.Navigate().GoToUrl(url);
        }

        public string PageSource()
        {
            return webDriver.PageSource;
        }
    }
}
